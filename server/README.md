#### Завдання

Написати веб-сервер на **Node.js** + **Express.js**, який може обробляти такі запити:
- **GET**: _/user_  
  отримання масиву всіх користувачів

- **GET**: _/user/:id_  
  отримання одного користувача по ID

- **POST**: _/user_  
  створення користувача за даними з тіла запиту

- **PUT**: _/user/:id_  
  оновлення користувача за даними з тіла запиту

- **DELETE**: _/user/:id_  
  видалення одного користувача по ID




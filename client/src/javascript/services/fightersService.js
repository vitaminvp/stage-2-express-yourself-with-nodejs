import { callApi } from "../helpers/apiHelper";

class FighterService {

  async getFighters() {
    try {
      const endpoint = "fighters";
      const apiResult = await callApi(endpoint, "GET");
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `fighters/${_id}`;
      const apiResult = await callApi(endpoint, "GET");
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
  async deleteFighters(_id) {
    try {
      const endpoint = `fighters/${_id}`;
      const apiResult = await callApi(endpoint, "DELETE");
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
